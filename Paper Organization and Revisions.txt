Original
--------
1	Introduction
2	Related Works
3	Our Contribution
4	Workshop Development and Preparation
	1 Recruitment, Objective, Schedule
	2 Initial Snap Policy and Implementation
	3 Curriculum Development (Sessions)
5	Workshop Aftermath
	1 Review of Sessions
6	Workshop Assessment
	1 Pre-Workshop Survey
	2 Post-Workshop Survey
	3 Teaching Effectiveness
	4 Learning Effectiveness (Pre-Post Test)
7	What Works and What Doesn't
	1 Works
	2 Doesn't work
8	Conclusions
9	Acknowledgements
10	References

Current
Revised
-------

1	Introduction
2	Related Works
3	Our Contribution
4	Workshop Development and Preparation
	1 Recruitment, Objective, Schedule
	2 Initial Snap Policy and Implementation
	3 Curriculum Development (Sessions)
5	Workshop Assessment
	1 Review of Sessions
	2 Pre-Workshop Survey
	3 Post-Workshop Survey
	4 Teaching Effectiveness (Interviews)
	5 Learning Effectiveness (Pre-Post Test)
6	What Works and What Doesn't
	1 Works
	2 Doesn't work
7	Conclusions
8	Acknowledgements
9	References